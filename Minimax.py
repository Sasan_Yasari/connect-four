import random
import time


class Minimax(object):
    board = None
    colors = ["x", "o"]

    def __init__(self, board, board_size):
        self.board = [x[:] for x in board]
        self.board_size = board_size

    def best_move(self, state, curr_player):

        start_time = time.time()
        if curr_player == self.colors[0]:
            opp_player = self.colors[1]
        else:
            opp_player = self.colors[0]

        legal_moves = {}
        for col in range(self.board_size):
            if self.is_legal_move(col, state):
                temp = self.make_move(state, col, curr_player)
                if self.board_size == 8:
                    legal_moves[col] = -self.search(3, temp, opp_player, -9999999)
                else:
                    legal_moves[col] = -self.search(4, temp, opp_player, -9999999)

        best_alpha = -99999999
        best_move = None
        moves = random.sample(legal_moves.items(), len(legal_moves.items()))
        for move, alpha in moves:
            if alpha >= best_alpha:
                best_alpha = alpha
                best_move = move

        print(time.time() - start_time)
        return best_move, best_alpha

    def search(self, depth, state, curr_player, prev_alpha):
        legal_moves = []
        for i in range(self.board_size):
            if self.is_legal_move(i, state):
                temp = self.make_move(state, i, curr_player)
                legal_moves.append(temp)

        if depth == 0 or len(legal_moves) == 0 or self.game_is_over(state):
            return self.value(state, curr_player, depth)

        if curr_player == self.colors[0]:
            opp_player = self.colors[1]
        else:
            opp_player = self.colors[0]

        alpha = -99999999
        for child in legal_moves:
            alpha = max(alpha, -self.search(depth - 1, child, opp_player, alpha))
            if prev_alpha > -alpha:
                return alpha
        return alpha

    def is_legal_move(self, column, state):
        for i in range(self.board_size - 1):
            if state[i][column] == ' ':
                return True
        return False

    def game_is_over(self, state):
        if self.check_for_streak(state, self.colors[0], 4) >= 1:
            return True
        elif self.check_for_streak(state, self.colors[1], 4) >= 1:
            return True
        else:
            return False

    def make_move(self, state, column, color):
        temp = [x[:] for x in state]
        for i in range(self.board_size - 1):
            if temp[i][column] == ' ':
                temp[i][column] = color
                return temp

    def value(self, state, color, depth):
        if color == self.colors[0]:
            o_color = self.colors[1]
        else:
            o_color = self.colors[0]

        my_fours = self.check_for_streak(state, color, 4)
        my_threes = self.check_for_streak(state, color, 3)
        my_twos = self.check_for_streak(state, color, 2)

        opp_fours = self.check_for_streak(state, o_color, 4)
        opp_threes = self.check_for_streak(state, o_color, 3)
        opp_twos = self.check_for_streak(state, o_color, 3)

        if opp_fours > 0:
            return -100000 - depth
        else:
            return my_fours * 100000 + my_threes * 100 + my_twos * 10 - opp_threes * 100 - opp_twos * 10 + depth

    def check_for_streak(self, state, color, streak):
        count = 0
        for i in range(self.board_size - 1):
            for j in range(self.board_size):
                if state[i][j].lower() == color.lower():
                    count += self.vertical_streak(i, j, state, streak)
                    count += self.horizontal_streak(i, j, state, streak)
                    count += self.diagonal_check(i, j, state, streak)
        return count

    def vertical_streak(self, row, col, state, streak):
        consecutive_count = 0
        for i in range(row, self.board_size - 1):
            if state[i][col].lower() == state[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= streak:
            return 1
        else:
            return 0

    def horizontal_streak(self, row, col, state, streak):
        consecutive_count = 0
        for j in range(col, self.board_size):
            if state[row][j].lower() == state[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= streak:
            return 1
        else:
            return 0

    def diagonal_check(self, row, col, state, streak):
        total = 0
        consecutive_count = 0
        j = col
        for i in range(row, self.board_size - 1):
            if j > self.board_size - 1:
                break
            elif state[i][j].lower() == state[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= streak:
            total += 1
        consecutive_count = 0
        j = col
        for i in range(row, -1, -1):
            if j > self.board_size - 1:
                break
            elif state[i][j].lower() == state[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= streak:
            total += 1

        return total
