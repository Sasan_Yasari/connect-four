
class HPlayer(object):

    def __init__(self, name, color, board_size):
        self.type = "Human"
        self.name = name
        self.color = color
        self.board_size = board_size

    def move(self, state):
        print("{0}'s turn.  {0} is {1}".format(self.name, self.color))
        column = None
        while column is None:
            try:
                choice = int(input("Enter a move (by column number): ")) - 1
            except ValueError:
                choice = None
            if 0 <= choice <= self.board_size-1:
                column = choice
            else:
                print("Invalid choice, try again")
        return column
