from Minimax import Minimax


class AIPlayer:
    def __init__(self, name, color, board_size):
        self.type = "AI"
        self.name = name
        self.color = color
        self.board_size = board_size

    def move(self, state):
        print("{0}'s turn.  {0} is {1}".format(self.name, self.color))
        m = Minimax(state, self.board_size)
        best_move, value = m.best_move(state, self.color)
        return best_move
